{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda.SQLite
import Repository
import Web.Scotty 
import Lucid
import TextShow
import Control.Monad.IO.Class (liftIO)

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do 
        html $ renderText $ viewHub

    get "/alldata" $ do
        threads <- liftIO $ withSQLite "data/ulco.db" selectAllThreads
        html $ renderText $ viewThreads threads
    

    {- get "/check_exist/:username" $ do 
        user_name <- param "username"
        exist <- userExist user_name
        if exist == True 
            then html $ renderText $ viewConnected 
        else html $ renderText $ viewNotConnected -}     

viewHub :: Html() 
viewHub = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "Hub"
        body_ $ do
            ul_ $ do
                li_ $ a_ [href_ "/alldata"] "alldata"

viewThreads :: [Thread] -> Html ()
viewThreads threads = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "all data"
        body_ $ do
            ul_ $ mapM_ viewThread threads

viewThread :: Thread -> Html ()
viewThread (Thread _ content )= li_ $ toHtml content 

viewConnected :: Html ()
viewConnected = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "UlcoForum"
        body_ $ p_ "Vous etes connecte"
                
viewNotConnected :: Html ()
viewNotConnected = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "UlcoForum"
        body_ $ p_ "Vous etes pas connecte"
        


