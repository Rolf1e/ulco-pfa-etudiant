{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Repository where 

import Database.Selda
import Database.Selda.SQLite
import TextShow


data Thread = Thread
  { thread_id :: ID Thread
  , content :: Text
  } deriving (Generic, Show)

data User = User
  { user_id   :: ID User
  , user_name :: Text
  } deriving (Generic, Show)

instance SqlRow User
instance SqlRow Thread

thread_table :: Table Thread
thread_table = table "thread" [#thread_id :- autoPrimary] 

user_table :: Table User
user_table = table "user" [#user_id :- autoPrimary]

selectAllThreads :: SeldaT SQLite IO [Thread]
selectAllThreads = query $ select thread_table

selectAllUsers :: SeldaT SQLite IO [User]
selectAllUsers = query $ select user_table

userExist :: String -> IO (Bool)
userExist user_name = do
  res <- withSQLite "data/ulco.db" selectAllUsers
  let parsed = map convert res
  return (user_name `elem` parsed)
 
convert :: User -> String
convert (User _ name) = show name



