CREATE TABLE user (
      user_id INTEGER PRIMARY KEY AUTOINCREMENT,
      user_name TEXT
);

CREATE TABLE thread (
      thread_id INTEGER PRIMARY KEY AUTOINCREMENT,
      content TEXT
);

INSERT INTO user VALUES(1, 'Tigran');
INSERT INTO user VALUES(2, 'Emma');
INSERT INTO user VALUES(3, 'Cassiopea');

INSERT INTO thread VALUES(1, 'one thread');
INSERT INTO thread VALUES(2, 'second thread');
INSERT INTO thread VALUES(3, 'third thread');