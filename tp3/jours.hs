
-- TODO Day
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday

getDay :: Day -> String
getDay Monday = "Monday"
getDay Tuesday = "Tuesday"
getDay Wednesday = "Wednesday"
getDay Thursday = "Thursday"
getDay Saturday = "Saturday"
getDay Sunday = "Sunday"

-- estWeekend :: Jour -> Bool
isWeekend :: Day -> Bool
isWeekend d =
  case d of
    Saturday -> True
    Sunday -> True
    _ -> False

-- compterOuvrables :: [Jour] -> Int
countOpen :: [Day] -> Int
countOpen [] = 0
countOpen (x:xs) | isWeekend(x) = 1 + countOpen(xs)
		 | otherwise = countOpen(xs)

countOpen2 :: [Day] -> Int
countOpen2 days = length [x | x <- days, not(isWeekend x)]

countOpen3 :: [Day] -> Int 
countOpen3 = length . filter (not . isWeekend)

