
-- TODO User
data User = User 
	{_name :: String
 	, _firstName :: String
  	, _age :: Int
 	}

-- showUser :: User -> String

showUser :: User -> String
showUser (User name firstName age) = name ++ " " ++ firstName ++ " " ++ (show age)

-- incAge :: User -> User
incAge :: User -> User
incAge (User name firstName age) = (User name firstName (age + 1))

main :: IO ()
main = putStrLn "TODO"

