import Source
import SiteBuilder

main :: IO ()
main = do
    sources <- loadJSON "data/genalbum.json"
    case sources of 
        Left e -> putStrLn e
        Right source -> print source
