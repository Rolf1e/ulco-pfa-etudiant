{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Source where 

import qualified Data.Text as T
import Data.Aeson
import GHC.Generics
import qualified Data.Text as T

data Source = Source 
    { imgs :: [T.Text]
    , url :: T.Text
    } deriving (Generic, Show)

instance FromJSON Source

loadJSON :: String -> IO (Either String [Source])
loadJSON jsonFile = eitherDecodeFileStrict jsonFile
