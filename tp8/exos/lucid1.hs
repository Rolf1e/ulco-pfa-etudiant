{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Data.Text.Lazy.IO

myHtml :: Html ()
myHtml = do
    h1_ "hello" 
    div_ $ do
        p_ "dfs"
        h2_ "ld"
    p_ "word" 

main :: IO ()
main = do
    print myHtml

