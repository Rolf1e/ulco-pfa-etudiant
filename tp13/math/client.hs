{-# LANGUAGE TypeApplications #-}

import Data.Proxy (Proxy(..))
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.Client 

import MathApi

query42 :: ClientM Int
query42 = client (Proxy @Math42)

-- queryAdd :: Int -> Int -> ClientM Int
-- TODO
queryAdd :: Int -> Int -> ClientM Int
queryAdd = client (Proxy @MathAdd)

-- queryMul2 :: Int -> ClientM Int
-- TODO
queryMul2 :: Int -> ClientM Int
queryMul2 = client $ Proxy @MathMul2

-- query42mul2 :: ClientM (Int, Int)
-- query42mul2 = do
-- TODO

query42mul2 :: ClientM (Int, Int)
query42mul2 = do
    res <- query42
    res2 <- queryMul2 2
    return (res, res2)

main :: IO ()
main = do
    myManager <- newManager defaultManagerSettings
    let myClient = mkClientEnv myManager (BaseUrl Http "localhost" 3000 "")

    runClientM query42 myClient >>= print
    runClientM (queryAdd 2 3) myClient >>= print
    runClientM (queryMul2 3) myClient >>= print
    runClientM query42mul2 myClient >>= print
    -- TODO

