{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module MathApi where

import Servant 

type Math42 = "quarante-deux" :> Get '[JSON] Int

-- TODO MathMul2
type MathMul2 = "mul2" :> Capture "x" Int :> Get '[JSON] Int

-- TODO MathAdd
type MathAdd = "add" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Int
