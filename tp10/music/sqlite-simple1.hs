{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple


selectAllArtists :: Connection -> IO [(Int,Text)]
selectAllArtists conn = query_ conn "SELECT * FROM artist"

main :: IO ()
main = withConnection "music.db" selectAllArtists >>= mapM_ print
