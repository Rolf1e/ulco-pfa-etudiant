{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

data Artist = Artist
  { artist_id   :: ID Artist
  , artist_name :: Text
  } deriving (Generic, Show)

data Title = Title
  { title_id :: ID Title
  , title_artist :: ID Artist
  ,title_name :: Text
  } deriving (Generic, Show)

instance SqlRow Artist
instance SqlRow Title

artist_table:: Table Artist
artist_table = table "artist" [#artist_id :- autoPrimary]

title_table:: Table Title
title_table = table "title" [#title_id :- autoPrimary
                              , #title_artist :- foreignKey artist_table #artist_id]

selectAllArtists:: SeldaT SQLite IO [Artist]
selectAllArtists = query $ select artist_table

-- selectAllData :: SeldaT SQLite IO [City :*: Country]
-- selectAllData = query $ do
--     cities <- select city_table
--     countries <- select country_table
--     restrict (cities ! #city_country .== countries ! #country_id)
--     return (cities :*: countries)

joinArtistsAndTitles :: SeldaT SQLite IO [Title :*: Artist]
joinArtistsAndTitles = query $ do
    titles <- select title_table
    artists <- select artist_table
    restrict (artists ! #artist_id .== titles ! #title_artist)
    return (titles :*: artists)
    

main :: IO ()
--main = withSQLite "music.db" selectAllArtists >>= mapM_ print
main = withSQLite "music.db" joinArtistsAndTitles >>= mapM_ print

