import Database.SQLite.Simple (open, close)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    putStrLn "\nMovie1.dbSelectAllMovies"
    
    selectAllMovies conn >>= mapM_ print


    putStrLn "\nMovie1.dbSelectAllProd"
    
    selectAllProds conn >>= mapM_ print


    close conn

