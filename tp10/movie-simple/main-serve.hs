{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import qualified Data.Text.Lazy as L
import Lucid
import Movie2
import Database.SQLite.Simple
import Web.Scotty 

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do 
        prods <- liftIO $ withConnection "movie.db"  Movie2.dbSelectAllProds
        html $ renderText $ viewMovies prods

viewMovies :: [Prod] -> Html ()
viewMovies prods = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "movie-simple"
        body_ $ do
            ul_ $ mapM_ viewProd prods
                

viewProd :: Prod -> Html ()
viewProd (Prod movie year person role) = li_ $ do 
    toHtml movie 
    toHtml $ show year 
    toHtml person 
    toHtml role
