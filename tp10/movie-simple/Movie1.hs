{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text (Text)
import Database.SQLite.Simple

selectAllMovies :: Connection -> IO [(Int, Text, Int)]
selectAllMovies conn = query_ conn "SELECT * FROM movie"

-- Movie1.dbSelectAllProds
-- ("Bernie",1996,"Albert Dupontel","R\233alisateur")
-- ...
-- 
-- Movie1.dbSelectMoviesFromPersonId 1
-- ["Citizen Kane"]
-- ...
--
-- joinArtists :: Connection -> IO [(Text, Text)]
-- joinArtists conn = query_ conn 
--     "SELECT artist_name, title_name FROM title \
--         \INNER JOIN artist ON title_artist = artist_id"
--
selectAllProds :: Connection -> IO [(Text, Int, Text, Text)]
selectAllProds conn = query_ conn
    "SELECT movie_title, movie_year, person_name, role_name FROM prod \
        \INNER JOIN movie ON prod_movie = movie_id \
        \INNER JOIN person ON prod_person = person_id \
        \INNER JOIN role ON prod_role = role_id"
 
    

