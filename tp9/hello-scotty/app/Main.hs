{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson
import Lucid
import Web.Scotty as WB
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import GHC.Generics

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving (Generic, Show)

instance ToJSON Person

indexPage :: Html ()
indexPage = do
    ul_ $ do
        li_ $ a_ [href_ "/route1"] "route1"
        li_ $ a_ [href_ "/route1/route2"] "route2"
        li_ $ a_ [href_ "/json1"] "json1"
        li_ $ a_ [href_ "/json2"] "json2"
        li_ $ a_ [href_ "/html1"] "html1"
        li_ $ a_ [href_ "/add1/20/10"] "add1"
        li_ $ a_ [href_ "/add2/20/10"] "add2"
        li_ $ a_ [href_ "/add2/?x=1"] "add2"
        li_ $ a_ [href_ "/index"] "index"
        img_ [src_ "bob.png"]
        

main :: IO ()
main = scotty 3000 $ do
    middleware $ staticPolicy $ addBase "static"
    get "/" $ html $ renderText indexPage
    get "/route1" $ text "this is /route1"
    get "/route1/route2" $ text "this is /route2"
    get "/html1" $ html "<h1>Page web</h1>"
    get "/json1" $ WB.json (42::Int)
    get "/json2" $ WB.json (Person "Bob" 21)
    get "/add1/:x/:y" $ do
        x <- param "x"
        let xnum = read x :: Int
        y <- param "y"
        let ynum = read y :: Int
        text $ L.pack $ show (xnum+ynum)
    get "/add2" $ do
        x <- param "x" `rescue` (\_ -> return "0")
        let xnum = read x :: Int
        y <- param "y" `rescue` (\_ -> return "0")
        let ynum = read y :: Int
        text $ L.pack $ show (xnum+ynum)
    get "/index" $ redirect "/"

