import Data.Text as T
import Data.Text.IO as T


main :: IO ()
main = do
    file <- T.readFile "yaml-test1.yaml"
    let content = T.unpack file 
    Prelude.putStrLn content
