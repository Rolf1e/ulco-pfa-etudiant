import qualified Data.Text.IO as T
import qualified Data.Text.Lazy.IO as L
import qualified Data.Text.Lazy as L


main :: IO ()
main = do
    file <- T.readFile "yaml-test1.yaml"
    let content = L.fromStrict file
    L.putStrLn content
