import qualified Data.ByteString.Char8 as C
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T


main :: IO ()
main = do
    file <- C.readFile "yaml-test1.yaml"
    let content = E.decodeUtf8 file
    T.putStrLn content
