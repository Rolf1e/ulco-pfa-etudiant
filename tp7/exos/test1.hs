import Data.ByteString.Char8 as C

main :: IO ()
main = do
    file <- C.readFile "yaml-test1.yaml"
    let content = C.unpack file 
    Prelude.putStrLn content
